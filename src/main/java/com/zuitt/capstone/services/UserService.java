package com.zuitt.capstone.services;

import com.zuitt.capstone.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    void createUser(User user);

    Iterable<User> getUsers();

    ResponseEntity deleteUser(int id);

    ResponseEntity updateUser(int id, User user);

    //Optional defines if the method may/may not return an object of the user class
    Optional<User> findByUsername(String username);
}
