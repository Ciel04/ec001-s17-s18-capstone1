package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    // Create course
    public void createCourse(Course course) {
        courseRepository.save(course);
    }

    // Get courses
    public Iterable<Course> getCourses() {
        return courseRepository.findAll();
    }
    @Override
    public Optional<Course> getCourseById(int id) {
        return Optional.empty();
    }

    // Delete course
    public ResponseEntity deleteCourse(int id) {
        courseRepository.deleteById(id);
        return new ResponseEntity<>("Course deleted successfully", HttpStatus.OK);
    }

    // Update course
    public ResponseEntity updateCourse(int id, Course course) {
        Course courseForUpdating = courseRepository.findById(id).get();

        courseForUpdating.setName(course.getName());
        courseForUpdating.setDescription(course.getDescription());
        courseRepository.save(courseForUpdating);
        return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
    }
    /*
    @Override
    public void enrollUser(int courseId, int userId) {

    }

    @Override
    public Iterable<Integer> getEnrollments(int courseId) {
        return null;
    }

    @Override
    public Iterable<Course> getActiveCourses() {
        return null;
    }

    @Override
    public ResponseEntity<String> addStudentToCourse(int courseId, int studentId) {
        return null;
    }

    @Override
    public ResponseEntity<String> removeStudentFromCourse(int courseId, int studentId) {
        return null;
    }

    @Override
    public Iterable<User> getStudentsInCourse(int courseId) {
        return null;
    }

    @Override
    public Boolean isCourseActive(int courseId) {
        return null;
    }
    */
    // Find course by name
    public Optional<Object> findByName(String name) {
        return Optional.ofNullable(courseRepository.findByName(name));
    }
}

