package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface CourseService {
    void createCourse(Course course);

    Iterable<Course> getCourses();

    Optional<Course> getCourseById(int id);

    ResponseEntity deleteCourse(int id);

    ResponseEntity updateCourse(int id, Course course);
    /*
    // Enroll user to a course
    void enrollUser(int courseId, int userId);

    // Get all enrollments for a course
    Iterable<Integer> getEnrollments(int courseId);

    // Get active courses
    Iterable<Course> getActiveCourses();

    ResponseEntity<String> addStudentToCourse(int courseId, int studentId);

    ResponseEntity<String> removeStudentFromCourse(int courseId, int studentId);

    Iterable<User> getStudentsInCourse(int courseId);

    Boolean isCourseActive(int courseId); */
}

