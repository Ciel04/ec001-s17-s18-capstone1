package com.zuitt.capstone.config;

import com.zuitt.capstone.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

//This annotation enables web securities in our application defined by WebSecurityConfig implementations
@Configuration
// This annotation enables the web securities defined by WebSecurityConfigurerAdapter automatically.
@EnableWebSecurity
// This annotation is used in Spring Security to enable global method-level security for protecting methods in your application.
// This is used to apply security measures at the method level, rather than just at the URL level.
// To perform authorization checks on specific methods based on the roles and privileges assigned to the user performing the action.
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    //injects the "JwtAuthenticate" class which implements the AuthenticateEntryPoint
    @Autowired
    private JwtAuthenticate jwtAuthenticate;

    //injects the "JwtUserDetailsService" class which handles the user details for JWT authentication
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    //injects the "JwtRequestFilter" class which has a custom filter for handling JWT requests
    //Filters to be applied on the request:
    //Jwt Authentication
    //username authentication
    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    //What to do during authentication
    //This serves as the authentication entry point for handling unauthorized requests

    //@Bean annotation is used in Spring Boot to make a special method that creates and manages an object that can be used in the different parts of the application

    @Bean
    public JwtAuthenticate jwtAuthenticateEntryPointBean() throws Exception{
        return new JwtAuthenticate();
    }

    //Instantiates a BCryptPasswordEncoder object for password hashing/encoding
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //This method is used in the configuration of Spring Security to expose the "AuthenticationManager" as a Bean on the Spring context.
    //By doing this, it can be injected into other parts of the application for authentication-related tasks
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    //Routes that will not require JWT tokens
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        //CORS - allows web browsers to make cross-origin HTTP requests from one domain to another domain
        //CSRF - is an attack that tricks a user into performing an action on a different website without their consent
        httpSecurity.cors().and().csrf().disable()
                .authorizeRequests()
                //.antMatchers() is used to configure the URL paths based on the permit requests and user role
                //allows the following endpoint to be accessible without requiring any authentication
                .antMatchers("/authenticate").permitAll()

                .antMatchers("/users/register").permitAll()

                //allows the "/posts" endpoint to be accessible via GET method without requiring any authentication
                .antMatchers(HttpMethod.GET, "/posts").permitAll()

                //allows unrestricted access to HTTP OPTIONS requests made to any URL in the application, regardless of the path, without requiring any authentication
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                //Specifies that any other requests not matching the above rules should be authenticated, meaning it requires proper authentication to be accessed
                .anyRequest().authenticated().and()

                //This configures the authentication entry point for handling authentication failures
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticate).and()

                //This is used to specify that the application should not create or use any sessions as it is following a statelesss authentication approach using JWT
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //This is used to add a custom filter(JwtRequestFilter) before a specific filter class (UsernamePasswordAuthenticationFilter)
        //"jwtRequestFilter" is responsible for handling JWT authentication and authorization logic.
        //"UsernamePasswordAuthenticationFilter" is a built-in filter in Spring Security that handles form-based authentication using username and password
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

}

