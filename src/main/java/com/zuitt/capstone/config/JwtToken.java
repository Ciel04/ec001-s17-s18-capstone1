package com.zuitt.capstone.config;

import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


// To generate token
@Component
public class JwtToken implements Serializable {

    //taken from application.properties
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID = -3629986839022159596L;

    //Time duration in seconds that the token can be used
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60; //5 hrs

    //This code generates a JWT by setting the claims, subject, issued-at time, expiration time, signing the JWT using a secret key and then compacting the JWT builder object to generate the final JWT String
    private String doGenerateToken(Map<String, Object> claims, String subject) {

        // Jwts.builder() creates a new JWT builder instance. This object is used to build and sign the JWT.
        // .setClaims includes the information to show the recipient which is the username
        // .setSubject adds information about the subject. (The subject username.)
        // .setIssuedAt sets the time and date when the token was created
        // .setExpiration sets the expiration of the token
        // .signWith creates the token using a declared algorithm, with the secret keyword.
        // "HS512" is a secure cryptographic algorithm.
        // The "secret key" is passed as a parameter and is used to verify the signature later on.
        // .compact() is used to generate the final JWT string by compacting the JWT builder object.
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000)).signWith(SignatureAlgorithm.HS512,secret).compact();
    }

    //This code generates a JWT token for a user by creating a map of claims, setting a "user" claim to the user's ID and then generating the token using the doGenerateToken method
    public String generateToken(UserDetails userDetails) {

        Map<String, Object> claims = new HashMap<>();

        //Retrieves the user information from the userRepository by using the username from the UserDetails object
        User user = userRepository.findByUsername(userDetails.getUsername());

        //This adds a claim in the "claims" map by setting a key to "user" and value of "user's ID. This claim is added to JWT that can be used to identify the user in the server
        claims.put("user", user.getId());

        //This invokes the doGenerateToken method and pass the "claims" and the user's username from the UserDetails object
        return doGenerateToken(claims, userDetails.getUsername());
    }

    //This code validates the token by extracting the username from the token
    public Boolean validateToken(String token, UserDetails userDetails){

        //Extract the username from the token and store it in a variable named token
        final String username = getUsernameFromToken(token);

        //Returns a Boolean value that indicates whether the token is valid or not
        //First condition checks if the "username" in the userDetails matches the username in the token
        //Second condition checks if the token is expired by invoking the "isTokenExpired()" method
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }


    // This method extracts a specific claim from a JWT token by using a functional interface called "Function<Claims, T>"
    // <T> T: This line defines a generic type T for the method, which will be used to specify the return type of the method.
    // "Function<Claims, T> claimsResolver": This line defines a functional interface called "claimsResolver", which takes in a "Claims" object and returns a generic type "T". The "Claims" object contains the set of claims contained in the JWT token.
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        //This calls "getAllClaimsFromToken" method to retrieve all the claims from the JWT token
        final Claims claims = getAllClaimsFromToken(token);

        //This extracts the specific claim value by using the "apply()" method
        return claimsResolver.apply(claims);
    }

    //This extracts all the claims from the token by taking the "token" String as an input
    private Claims getAllClaimsFromToken(String token) {

        // Jwts.parser() creates a new parser instance, which is used to parse a JWT token.
        // .setSigningKey() sets the secret key used to sign the token.
        // .parseClaimsJws() parses the JWT token specified by the token parameter and returns a Jws<Claims> object.
        // The Jws object contains the header and body of the JWT token, as well as the signature.
        // .getBody() contains all the claims that were included in the JWT token (claims are usually placed inside the JWT body).
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    //This is used to extract the subject(username) from the JWT token
    public String getUsernameFromToken(String token){

        // "claim" variable will store the extracted claim from the JWT Token
        // "Claims::getSubject" method reference is used to extract the subject (i.e., username) claim from the JWT token.
        String claim = getClaimFromToken(token, Claims::getSubject);
        return claim;
    }

    //This is used to extract the expiration date of the token
    public Date getExpirationDateFromToken(String token){

        // "Claims::getExpiration" method reference is used to extract the expiration time claim from the JWT token.
        return getClaimFromToken(token, Claims::getExpiration);
    }

    //This is used to check if the JWT token has expired
    private Boolean isTokenExpired(String token){


        // getExpirationDateFromToken() invokes the "getExpirationDateFromToken()" method with the "token" as parameter to retrieve expiration date which is placed inside the "expiration" variable
        final Date expiration = getExpirationDateFromToken(token);

        // This returns a boolean value that represents whether the current time (new Date()) is "before" the expiration date of the JWT Token (expiration)
        // If the current time is before the expiration time, it will return false.
        // If the current time is after the expiration time, it will return true.
        return expiration.before(new Date());
    }
}

