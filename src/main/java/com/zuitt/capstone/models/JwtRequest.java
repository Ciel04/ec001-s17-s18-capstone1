package com.zuitt.capstone.models;

import java.io.Serializable;

//The JwtRequest model is used in creating a JWT using the request body contents for the payload in th "AuthController" file
public class JwtRequest implements Serializable {

    //Properties
    private static final long serialVersionUID = 7259464237270634705L;

    private String username;

    private String password;

    //Constructors
    //Default constructors
    public JwtRequest(){

    }

    //Parameterized Constructor
    public JwtRequest(String username, String password){
        this.setUsername(username);
        this.setPassword(password);
    }

    //Getters and Setters
    public String getUsername(){
        return this.username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return this.password;
    }

    public void setPassword(String password){
        this.password = password;
    }
}

