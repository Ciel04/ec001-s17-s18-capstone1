package com.zuitt.capstone.models;


import java.io.Serializable;

//This will be used for sending the generated JWT token string as a response in the "AuthController
public class JwtResponse  implements Serializable {

    private static final long serialVersionUID = 6313575183494676360L;

    //Properties
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    //Getters and Setters

    public String getToken(){
        return this.jwttoken;
    }
}

