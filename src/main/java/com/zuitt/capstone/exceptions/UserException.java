package com.zuitt.capstone.exceptions;

public class UserException extends Exception{
    public UserException(String message){
        //"super()" can be used to invoke immediate parent class constructor
        super(message);
    }
}